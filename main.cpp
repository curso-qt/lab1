#include <iostream>
#include <vector>

using namespace std;

struct Produto{
    string nome;
    float preco;
};

struct Compra{
    string cliente;
    vector<Produto> produtos;
};

void totalCompra(const Compra &compra){
    float sum = 0.0;
    for (const auto &produto : compra.produtos){
        sum += produto.preco;
    }
    cout << "Valor total da compra: " << sum << endl;
}

void cancelarCompra(Compra &compra){
    compra.produtos.clear();
    compra.cliente.clear();
}

auto main() -> int
{
    //Nessa aula foi demonstrado:
    //Como usar atribuições usando o c++ moderno (v 11+)
    //Exemplos de chamada por referencia constante
    Compra compra {"Edivaldo", {
            {"Chocolate em pó", 10.0},
            {"Leite liquido", 4.59},
            {"Creme de leite", 2.49}
    }};

    cout << "Nome primeiro ingrediente: \n" << compra.produtos[0].nome << endl;

    //imprimindo total da compra
    totalCompra(compra);
    cancelarCompra(compra);

    cout << "Nome cliente: " << compra.cliente << " - " << compra.produtos.size() << endl;

    return 0;
}
